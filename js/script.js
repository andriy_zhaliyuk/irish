$(document).ready(function () {
    $(".product").slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow:2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $(".video-product__wrap").on("click", function () {
        let $this = $(this);
        let $iframe = $this.find("iframe");
        let src = $iframe.attr('src');
        $this.find(".video-image").css({"display": "none"});
        $iframe.css({"display": "block"});
        $iframe.attr('src', src + '&autoplay=1');
    });

    $('.toogle-mobile-menu').on("click", function(e) {
        e.preventDefault();
        $('body').toggleClass('menu-opened');
    });

});